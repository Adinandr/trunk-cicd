# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.1](https://gitlab.com/Adinandr/trunk-cicd/compare/v1.1.0...v1.1.1) (2021-07-18)


### Bug Fixes

* oops what happened? ([bc90f8a](https://gitlab.com/Adinandr/trunk-cicd/commit/bc90f8ae99d102f7708c8ed4cd024a7a68b85cf5))

## [1.1.0](https://gitlab.com/Adinandr/trunk-cicd/compare/v1.0.1...v1.1.0) (2021-07-18)


### Features

* some feature ([5d43cf4](https://gitlab.com/Adinandr/trunk-cicd/commit/5d43cf4a6f1a909ad7675ee530472e1cccca9ea1))

### 1.0.1 (2021-07-18)
